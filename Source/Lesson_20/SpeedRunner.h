// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "SpeedRunner.generated.h"

class ASnakeBase;
class UStaticMeshComponent;
UCLASS()
class LESSON_20_API ASpeedRunner : public AActor, public  IInteractable

{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpeedRunner();

		

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	

	
};
