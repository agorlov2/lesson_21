// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lesson_20GameModeBase.generated.h"

/**
 *
 */
class ASpeedRunner;
UCLASS()
class LESSON_20_API ALesson_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedRunner> SpeedRunnerType;
	UPROPERTY(EditDefaultsOnly)
		ASpeedRunner* wwww1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	FTimerHandle MembernimerHandle;

	void SpawnSpeedBonus();
};