// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestActor.generated.h"

UENUM(BlueprintType)
enum class EEcample:uint8
{
	E_RED   UMETA(displayname="RED"),
	E_GREEN UMETA(displayname="GREEN"),
	E_BLUE	UMETA(displayname="BLUE")
};
UCLASS()
class LESSON_20_API ATestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();
	UPROPERTY(BlueprintReadOnly)
		EEcample ExampleElementValue;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
