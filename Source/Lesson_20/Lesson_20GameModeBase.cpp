// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lesson_20GameModeBase.h"
#include "SpeedRunner.h"
//#include "Kismet/GameplayStatics.h"

void ALesson_20GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(MembernimerHandle,this, &ALesson_20GameModeBase::SpawnSpeedBonus, 10.0f, true, 5.0f);
}

//Spawn SpeedRunner
void ALesson_20GameModeBase::SpawnSpeedBonus()
{

	int32 RandomCounterMin = -480;
	int32 RandomCounterMax = 480;
	int32 RandomX = FMath::FRandRange(RandomCounterMin, RandomCounterMax);
	int32 RandomY = FMath::FRandRange(RandomCounterMin, RandomCounterMax);

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
	//								FString::Printf(TEXT("Some variable valuesk RandomX: %i, RandomY: %i,NumSpeedRunner: %i"),
	//								RandomX, RandomY, NumSpeedRunner));

	FVector NewLocation1(RandomX, RandomY, 10);
	FTransform NewTransform1(NewLocation1);
	ASpeedRunner* NewSpeedRunner = GetWorld()->SpawnActor<ASpeedRunner>(ALesson_20GameModeBase::SpeedRunnerType, NewTransform1);
	
}
