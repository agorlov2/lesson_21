// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedRunner.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Lesson_20GameModeBase.h"





//#include "TimeManager.h"

// Sets default values
ASpeedRunner::ASpeedRunner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	
	
}

// Called when the game starts or when spawned
void ASpeedRunner::BeginPlay()
{
	Super::BeginPlay();
		
	//GetWorldTimerManager().SetTimer(MembernimerHandle, this, &ASpeedRunner::RepeatingFunction, 10.0f, false, 5.0f);
}

// Called every frame
void ASpeedRunner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedRunner::Interact(AActor* Interactor, bool bIsHead)
{
	static int32 NumSpeedRunner = 30;

	ASnakeBase* TouchSpeedRunner = Cast<ASnakeBase>(Interactor);
	if (IsValid(TouchSpeedRunner))

	{
		ALesson_20GameModeBase* SnakeGameMode = Cast<ALesson_20GameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if (SnakeGameMode)
		{
			SnakeGameMode->SpawnSpeedBonus();
		}
		//TouchSpeedRunner->SetActorHiddenInGame(false);
		TouchSpeedRunner->MoveSpeed = TouchSpeedRunner->MoveSpeed * 0.9f;

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red,
		//FString::Printf(TEXT("Some variable valuesk NumSpeedRunner: %i"),
		//NumSpeedRunner));

		
		Destroy();
	}

}

