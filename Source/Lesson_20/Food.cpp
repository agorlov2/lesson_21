// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementsBase.h"
#include "Wall.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{
						
			Snake->AddSnakeElement(1);
			Destroy();
			int32 RandomCounter = 480;
			int32 RandomX = rand() % RandomCounter;
			int32 RandomY = rand() % RandomCounter;
			if (rand() % RandomCounter >= RandomCounter / 2)
			{
				RandomX = -RandomX;
			}
			if (rand() % RandomCounter >= RandomCounter / 2)
			{
				RandomY = -RandomY;
			}

			FVector NewLocation1(RandomX, RandomY, 0);
			FTransform NewTransform1(NewLocation1);
			AFood* SpawnFood = GetWorld()->SpawnActor<AFood>(GetClass(), NewTransform1);
			auto FoodTouchWall = Cast<AWall>(SpawnFood);

			//GEngine->AddOnScreenDebugMessage(-5, 5.f, FColor::Red,
			//	FString::Printf(TEXT("FoodTouchWall: %i"),
			//		FoodTouchWall));

			while (IsValid(FoodTouchWall))
			{
				Destroy();
				SpawnFood = GetWorld()->SpawnActor<AFood>(GetClass(), NewTransform1);
				FoodTouchWall = Cast<AWall>(SpawnFood);
			}


			//Afood* SpawnFood = GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementsClass, NewTransform);

			//ASnakeElementsBase* NewSnakeElem = GetWorld()->SpawnActor<AFood>(AFood,);
			
		}
	}
}

